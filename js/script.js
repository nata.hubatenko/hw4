function calc() {
	let firstNum = +prompt('Enter first number');
	while (isNaN(firstNum)) {
		firstNum = +prompt('Enter first number');
	}

	let secondNum = +prompt('Enter second number');
	while (isNaN(secondNum)) {
		secondNum = +prompt('Enter second number');
	}

	let operation = prompt('Choose the operation: +, -, /, *');
	while (operation !== '+' && operation !== '-' && operation !== '*' && operation !== '/') {
		operation = prompt('Choose the operation: +, -, /, *');
	}

	let result;
	switch (operation) {
		case '+':
			result = firstNum + secondNum;
			break;
		case '-':
			result = firstNum - secondNum;
			break;
		case '*':
			result = firstNum * secondNum;
			break;
		case '/':
			if (secondNum === 0) {
				return 'Division by zero';
			}
			result = firstNum / secondNum;
			break;
	}

	return result;
}

console.log(calc());

























